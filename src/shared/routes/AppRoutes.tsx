import { Routes, Route } from "react-router-dom";
import { ReactSpinner, FormInput } from '../components'
import { Users } from "../../users";

export const AppRoutes = () => {

    return (
        <Routes>

            <Route path="/" element={
                <>
                    <ReactSpinner/>
                </>
            }>
            </Route>

            <Route path="/login" element={
                <>
                    <FormInput/>
                </>
            }>
            </Route>

            <Route path="/users" element={
                <>
                    <Users/>
                </>
            }>
            </Route>
            
        </Routes>
    );
}
