import { Container, Form, Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export const FormInput = () => {
    return (
        <Container>
            <Row className='justify-content-center'>
                <Col xs={6}>
                    <Form className='mt-5'> 

                        <Form.Group className="text-start mb-3" controlId="formName">
                            <Form.Label> Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter your name" />
                        </Form.Group>

                        <Form.Group className="text-start mb-3" controlId="formSurname">
                            <Form.Label>Surname</Form.Label>
                            <Form.Control type="text" placeholder="Enter your surname" />
                        </Form.Group>

                        <Form.Group className="text-start mb-3" controlId="formEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group className="text-start mb-3" controlId="formPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>

                        <Link to="/users">
                            <Button variant="primary" >Login</Button>
                        </Link>
                        
                    </Form>


                </Col>


            </Row>


        </Container>

    );
}