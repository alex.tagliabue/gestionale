import React from 'react';
import logo from '../../../../logo.svg';
import '../../../../App.css';

function ReactSpinner() {
  return (
      <main className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </main>
  );
}

export { ReactSpinner };
