import { Table, Button } from 'react-bootstrap'
import { UserRaw } from '../../components'
import { useMemo } from 'react';


export const Users = () => {

    let UsersList = [
        {
            id:1,
            name: 'Mario',
            surname: 'Rossi',
            email: 'mario.rossi@gmail.it',
            password: '1234!'
        },
        {
            id:2,
            name: 'Carlo',
            surname: 'Bianchi',
            email: 'carlo.bianchi@gmail.it',
            password: '1234!'
        },

    ]

    const showUsersList = useMemo(() => {
        return UsersList.map((user) => <UserRaw key={user.id} user={user} />)
    }, [UsersList]);

    return (
        <>
            <Button>Add</Button>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Actions</th>

                    </tr>
                </thead>

                {showUsersList}

            </Table>
        </>

    );
}

