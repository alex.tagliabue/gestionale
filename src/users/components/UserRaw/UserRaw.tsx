import { User } from "../../types";
import { Button, Modal } from 'react-bootstrap';
import * as Icon from 'react-bootstrap-icons';
import { useState } from "react";
//import { UserModal } from "..";

export const UserRaw = (prop: { user: User }) => {
    
    const user = prop.user;

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <tbody>

                <tr>
                    <td>{user.name}</td>
                    <td>{user.surname}</td>
                    <td>{user.email}</td>
                    <td>
                        <Button><Icon.Trash /> </Button>
                        <Button onClick={()=>handleShow()}> <Icon.Pencil /> </Button>
                        <Button onClick={()=>handleShow()}> <Icon.Eye /> </Button>
                    </td>
                </tr>

            </tbody>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <div>
                        <Modal.Title>Hi {user.name}</Modal.Title>
                    </div>
                </Modal.Header>

                <Modal.Body>
                    <h5>Your details:</h5>
                    <div>Name: {user.name}</div>
                    <div>Surname: {user.surname}</div>
                    <div>Email: {user.email}</div>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>

    );
}