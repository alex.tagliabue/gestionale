export type User = {
    id: number,
    name: string,
    surname: string,
    email: string,
    password: string
} 

export type ShowModal = {
    showModal: true;
}