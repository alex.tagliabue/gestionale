import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom'
import { AppRoutes } from './shared/routes'
import { NavbarCustom } from './shared/components';

import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
      <NavbarCustom/>
      <AppRoutes></AppRoutes>

      </div>
    </Router>

  );
}

export default App;
